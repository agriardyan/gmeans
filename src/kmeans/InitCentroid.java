/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kmeans;

import gmeans.Calculation;
import gmeans.Utility;
import java.util.Arrays;

/**
 *
 * @author pacman
 */
public class InitCentroid {

    public InitCentroid() {
    }
    
    /**
     * Mencari median dari data matrix 2d
     * @param data matrix 2d
     * @return matrix 1d berisi median tiap kolom
     */
    public double[] median(double[][] data) {
        Utility util = new Utility();
        double[] med = new double[data[0].length];
        
        for (int i = 0; i < data[0].length; i++) {
            double[] sort = util.fetchColumnMatrix2D(data, i);
            
            Arrays.sort(sort);
            int medPoint = sort.length / 2;
            if(sort.length%2 == 0) {
                med[i] = (sort[medPoint - 1] + sort[medPoint]) / 2;
            } else {
                med[i] = sort[medPoint];
            }
        }
        
        return med;        
    }
    
    public double[] initCentroidFromData(double[][] data) {
        Calculation calc = new Calculation();
        double[] median = median(data);
        
        double minDist = Double.MAX_VALUE;
        double tempDist = 0;
        int minIndex = 0;
        
        for (int i = 0; i < data.length; i++) {
            tempDist = calc.euclideanDistance(data[i], median);
            if(tempDist < minDist) {
                minDist = tempDist;
                minIndex = i;
            }
        }
        
        return data[minIndex];
        
    }
    
}
