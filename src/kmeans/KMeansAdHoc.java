/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kmeans;

import gmeans.AutoGeneratedID;
import gmeans.Calculation;
import gmeans.Cluster;
import gmeans.Record;
import gmeans.Utility;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pacman
 */
public class KMeansAdHoc {

    public KMeansAdHoc() {
    }

    public Cluster[] runKMeans(Cluster cluster, double[] initCentroid1, double[] initCentroid2) {

        Utility util = new Utility();
        Calculation calc = new Calculation();

        int index = 0;
        Cluster activeCluster = cluster;
        
        double[] centroid1 = initCentroid1;
        double[] centroid2 = initCentroid2;

        Cluster cluster1 = new Cluster();
        Cluster cluster2 = new Cluster();
        
        // isi centroid awal
        cluster1.centroid = util.convert1DArrayToRecord(centroid1);
        cluster2.centroid = util.convert1DArrayToRecord(centroid2);
        
        // beri nama cluster secara random
        cluster1.clusterID = AutoGeneratedID.generateRandomString();
        cluster2.clusterID = AutoGeneratedID.generateRandomString();
        
        // assign ke variabel recordList (gak penting, hanya saja kadung tdk bisa refactor rename)
        List<Record> recordList = activeCluster.recordList;
        
        // buat list penampungan record yang telah diassign ke centroid terdekat (sudah ditemukan clusternya)
        List<Record> tempRecordListCluster1 = null;
        List<Record> tempRecordListCluster2 = null;

        a : while (true) {
            
            tempRecordListCluster1 = new ArrayList<>();
            tempRecordListCluster2 = new ArrayList<>();
            
            // assign ke centroid terdekat
            for (int j = 0; j < recordList.size(); j++) {
                Record rec = recordList.get(j);
                double[] dataArray = util.convert1DListToArray(rec);
                double distToCentroid1 = calc.euclideanDistance(dataArray, centroid1);
                double distToCentroid2 = calc.euclideanDistance(dataArray, centroid2);

                if (distToCentroid1 > distToCentroid2) {
                    tempRecordListCluster2.add(rec);
                } else {
                    tempRecordListCluster1.add(rec);
                }
            }
            
            // hitung centroid baru
            centroid1 = calculateNewCentroid(tempRecordListCluster1);
            centroid2 = calculateNewCentroid(tempRecordListCluster2);
            
            // cek centroid baru thd centroid lama, apabila centroidBaru = centroidLama, maka kmeans selesai (stop while loop)
            boolean checkCluster1 = checkNewWithOldCentroid(centroid1, util.convert1DListToArray(cluster1.centroid));
            boolean checkCluster2 = checkNewWithOldCentroid(centroid2, util.convert1DListToArray(cluster2.centroid));
            
            // pindahkan isi tempRecordListCluster ke recordList di Cluster yang sesuai
            cluster1.recordList = tempRecordListCluster1;
            cluster1.centroid = util.convert1DArrayToRecord(centroid1);
            cluster2.recordList = tempRecordListCluster2;
            cluster2.centroid = util.convert1DArrayToRecord(centroid2);
            
            if(checkCluster1 && checkCluster2) {
                break a;
            }
        }
        
        Cluster[] arrayCluster = new Cluster[2];
        arrayCluster[0] = cluster1;
        arrayCluster[1] = cluster2;
        
        return arrayCluster;

    }
    
    public boolean checkNewWithOldCentroid(double[] newCentroid, double[] oldCentroid) {
                
        if (oldCentroid.length != newCentroid.length) {
            throw new IllegalArgumentException();
        }
        
        Calculation calc = new Calculation();
        
        boolean stat = false;
        
        double dist = calc.euclideanDistance(newCentroid, oldCentroid);
        
        if(dist == 0) {
            stat = true;
        } else {
            stat = false;
        }
        
        return stat;
    }
    
    public double[] calculateNewCentroid(List<Record> recordList) {
        int rowCount = recordList.size();
        double[] colSum = new double[recordList.get(0).dataList.size()];
        for (int i = 0; i < recordList.size(); i++) {
            for (int j = 0; j < recordList.get(i).dataList.size(); j++) {
                colSum[j] += recordList.get(i).dataList.get(j);
            }
        }
        
        double[] newCentroid = new double[colSum.length];
        for (int i = 0; i < colSum.length; i++) {
            newCentroid[i] = colSum[i] / rowCount;
        }
        
        return newCentroid;
    }

    

}
