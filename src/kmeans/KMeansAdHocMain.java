/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kmeans;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import gmeans.CSVLoader;
import gmeans.Calculation;
import gmeans.Cluster;
import gmeans.Record;
import gmeans.Utility;
import java.util.List;

/**
 *
 * @author pacman
 */
public class KMeansAdHocMain {

    public static void main(String[] args) {

        CSVLoader csv = new CSVLoader();
        Cluster cluster = csv.loadCSVToCluster("./data/iris_id.csv");

        Utility util = new Utility();
        double[][] valueMatrix = util.convert2DListToMatrix(cluster);
        Matrix m = new Matrix(valueMatrix);

        double[] clusterCenter = {5.7, 3, 4.2, 1.2};

        EigenvalueDecomposition eig = m.eig();
        Matrix eigenValues = eig.getD();
        Matrix eigenVectors = eig.getV();

        double[][] eigenValueArrays = eigenValues.getArray();
        double[][] eigenVectorArrays = eigenVectors.getArray();

        double eigenValue = eigenValueArrays[0][0];
        double[] eigenVector = util.fetchColumnMatrix2D(eigenVectorArrays, 0);

        Calculation calc = new Calculation();
        
        double[] initCentroid1 = calc.calculateChildCentroidPositive(clusterCenter, eigenVector, eigenValue);
        double[] initCentroid2 = calc.calculateChildCentroidNegative(clusterCenter, eigenVector, eigenValue);

        KMeansAdHoc kmean = new KMeansAdHoc();
        Cluster[] runKMeans = kmean.runKMeans(cluster, initCentroid1, initCentroid2);
        
        for (int i = 0; i < runKMeans.length; i++) {
            Cluster c = runKMeans[i];
            
            String res = c.toString();
            
            System.out.println(res);
            
        }
        

    }

}
