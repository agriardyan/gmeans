/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gmeans;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author root
 */
public class Cluster {
    
    public String clusterID;
    public List<Record> recordList;
    public Record centroid;
    public boolean isStable;

    public Cluster() {
        recordList = new ArrayList<>();
    }

    @Override
    public String toString() {
        String title = "---- Cluster " + clusterID + " ----\n";
        String subtitle = "---- centroid : ";
        for (int i = 0; i < centroid.dataList.size(); i++) {
            subtitle += centroid.dataList.get(i).toString() + ", ";
        }
        subtitle += "\n";
        String content = "";
        for (int i = 0; i < recordList.size(); i++) {
            content += recordList.get(i).toString() + "\n";
        }
        return title + subtitle + content;
    }
    
    
    
    
    
}
