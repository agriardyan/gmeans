/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gmeans;

import kmeans.InitCentroid;

/**
 *
 * @author pacman
 */
public class GMeansMain {

    public static void main(String[] args) {
        GMeansAlgorithm r = new GMeansAlgorithm();
        CSVLoader csv = new CSVLoader();
        Cluster cs = csv.loadCSVToCluster("./data/iter/flight_id.csv");
        
        InitCentroid ic = new InitCentroid();

        ClusterPool pool = new ClusterPool();

        pool.clusterList.add(cs);

        int poolIndex = 0;

        a:
        while (true) {

            b : while (true) {
                Utility util = new Utility();

                Cluster cluster = pool.getClusterAt(poolIndex);
                
                if(cluster == null) {
                    break b;
                }

                double[] clusterCenter = ic.initCentroidFromData(util.convert2DListToMatrix(cluster));

                cluster.centroid = util.convert1DArrayToRecord(clusterCenter);

                double alpha = r.step1();
                double[][] initCentroidSet = r.step2(cluster);
                Cluster[] clusterSet = r.step3(cluster, initCentroidSet[0], initCentroidSet[1]);
                double[] xiac = r.step4(util.convert1DListToArray(clusterSet[0].centroid), util.convert1DListToArray(clusterSet[1].centroid), cluster);

                System.out.println("---- xiac");
                for (double xic : xiac) {
                    System.out.print(xic + " ");
                }

                System.out.println("");

                boolean isStable = r.step5(xiac, alpha);

                if (isStable) {
                    System.out.println("not need split");
                    cluster.isStable = true;
                    poolIndex = poolIndex + 1;
                } else {
                    System.out.println("need split");
                    cluster.isStable = false;
                    clusterSet[0].isStable = false;
                    clusterSet[1].isStable = false;
                    poolIndex = pool.replaceOneClusterWithTwoAt(poolIndex, clusterSet[0], clusterSet[1]);
                }

                for (int i = 0; i < clusterSet.length; i++) {
                    Cluster c = clusterSet[i];

                    String res = c.toString();

                    System.out.println(res);
                }
                
                if(poolIndex == -1 ) {
                    break b;
                }
                
            }
            
            System.out.println("break");

            // cek cluster pool, apakah ada yg perlu displit lagi
            boolean allStable = true;
            c:for (int i = 0; i < pool.clusterList.size(); i++) {
                Cluster get = pool.clusterList.get(i);
                if (get.isStable == false) {
                    allStable = false;
                    break c;
                }
            }

            // jika semua sudah stable (tdk perlu split, maka hentikan loop while)
            if (allStable) {
                break a;
            } else {
                poolIndex = 0;
            }
        }
        
        System.out.println("\n\n");
        System.out.println(" -------- RESULT ----------- ");
        int poolSize = pool.clusterList.size();
        for (int i = 0; i < poolSize; i++) {
            System.out.println(pool.clusterList.get(i).toString());
        }

    }

}
