/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gmeans;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

/**
 *
 * @author pacman
 */
public class Calculation {

    public Calculation() {
    }
    
    public double dotProduct(double[] x, double[] y) {
        if(x.length != y.length) {
            throw new IllegalArgumentException();
        }
        
        double sum = 0;
        
        for (int i = 0; i < x.length; i++) {
            sum += (x[i] * y[i]);
        }
        
        return sum;
        
    }
    
    public double[] zScoreNormalization(double[] data) {
        DescriptiveStatistics ds = new DescriptiveStatistics(data);
        double mean = ds.getMean();
        double stdev = ds.getStandardDeviation();
        
        double[] result = new double[data.length];
        
        for (int i = 0; i < data.length; i++) {
            result[i] = (data[i] - mean) / stdev;
        }
        
        return result;
        
    }
    
    public double calculateCDF(double data) {
        NormalDistribution n = new NormalDistribution();
        return n.cumulativeProbability(data);
    }
    
    public double[] calculateCDF(double[] data) {
        NormalDistribution n = new NormalDistribution();
        double[] result = new double[data.length];
        
        for (int i = 0; i < data.length; i++) {
            result[i] = n.cumulativeProbability(data[i]);
        }
        
        return result;
    }

    public double[] calculateChildCentroidPositive(double[] clusterCenter, double[] eigenVector, double eigenValue) {
        int lengthCluster = clusterCenter.length;
        int lengthEigVec = eigenVector.length;

        double[] newEigenVector = null;
        double[] child = null;

        if (lengthCluster == lengthEigVec) {
            newEigenVector = new double[lengthEigVec];
            for (int i = 0; i < lengthEigVec; i++) {
                newEigenVector[i] = eigenVector[i] * Math.sqrt((2 * eigenValue) / Math.PI);
            }
            
            child = new double[lengthEigVec];
            for (int i = 0; i < lengthCluster; i++) {
                child[i] = clusterCenter[i] + newEigenVector[i];
            }
        } else {
            throw new IllegalArgumentException();
        }

        return child;
    }

    public double[] calculateChildCentroidNegative(double[] clusterCenter, double[] eigenVector, double eigenValue) {
        int lengthCluster = clusterCenter.length;
        int lengthEigVec = eigenVector.length;

        double[] newEigenVector = null;
        double[] child = null;

        if (lengthCluster == lengthEigVec) {
            newEigenVector = new double[lengthEigVec];
            for (int i = 0; i < lengthEigVec; i++) {
                newEigenVector[i] = eigenVector[i] * Math.sqrt((2 * eigenValue) / Math.PI);
            }

            child = new double[lengthEigVec];
            double tempData = 0;
            for (int i = 0; i < lengthCluster; i++) {
                child[i] = clusterCenter[i] - newEigenVector[i];
            }
        } else {
            throw new IllegalArgumentException();
        }

        return child;
    }
    
    public double euclideanDistance(double[] data, double[] centroid) {
        if (data.length != centroid.length) {
            throw new IllegalArgumentException();
        }

        double sum = 0;

        for (int i = 0; i < data.length; i++) {
            sum += Math.pow(data[i] - centroid[i], 2);
        }

        return Math.sqrt(sum);

    }

}
