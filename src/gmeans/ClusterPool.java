/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gmeans;

import java.util.LinkedList;

/**
 *
 * @author root
 */
public class ClusterPool {

    public LinkedList<Cluster> clusterList;

    public ClusterPool() {
        clusterList = new LinkedList<>();
    }

    public void addFirst(Cluster e) {
        clusterList.addFirst(e);
    }

    public Cluster getClusterAt(int index) {
        try {
            return clusterList.get(index); 
        } catch (IndexOutOfBoundsException ex) {
            return null;
        }
        
    }

    /**
     * Mereplace satu slot dalam linkedlist (yang berisi Cluster) dengan dua
     * slot (masing-masing berisi Cluster) kemudian menggeser slot lama ke kanan
     * Misal diketahui suatu linkedlist -> a, d, g Apabila diinsert
     * replaceOneClusterWithTwoAt(2, b, c) maka list akan menjadi -> a, b, c, g
     *
     * @param index index linkedlist yang akan diinsert
     * @param cluster1 instance Cluster yang pertama
     * @param cluster2 instance Cluster yang kedua
     * @return index slot lama yang tergeser, apabila return -1 berarti tadi
     * terjadi replace di ujung list
     */
    public int replaceOneClusterWithTwoAt(int index, Cluster cluster1, Cluster cluster2) {
        int nextIndex = -1;

        try {
            if (clusterList.get(index + 1) != null) {
                nextIndex = index + 2;
            }
        } catch (IndexOutOfBoundsException ex) {
            nextIndex = -1;
        }

        replaceAt(index, cluster1);
        insert(index + 1, cluster2);

        return nextIndex;
    }

    private void insert(int index, Cluster e) {
        clusterList.add(index, e);
    }

    private void replaceAt(int index, Cluster e) {
        clusterList.set(index, e);
    }

}
