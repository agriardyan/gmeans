/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gmeans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author root
 */
public class CSVLoader {

    public CSVLoader() {
    }
    
    public Cluster loadCSVToCluster(String path) {
        
        File fin = new File(path);
        
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(fin);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CSVLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
 
	//Construct BufferedReader from InputStreamReader
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
 
	String line = null;
        String[] dataPoints = null;
        
        Record rec = null;
        Cluster cluster = new Cluster();
        
        try {
            while ((line = br.readLine()) != null) {
                rec = new Record();
                dataPoints = line.split(",");
                double dtpts = 0;
                
                for (int i = 0; i < dataPoints.length; i++) {
                    if(i == 0) {
                        rec.recordID = dataPoints[i];
                    } else if (i > 0 && i < dataPoints.length - 1) {
                        dtpts = Double.parseDouble(dataPoints[i]);
                        rec.dataList.add(dtpts);
                    } else {
                        rec.c = dataPoints[i];
                    }
                }
                
//                for (String dataPoint : dataPoints) {
//                    dtpts = Double.parseDouble(dataPoint);
//                    
//                    // auto increment id record
//                    rec.recordID = AutoIncrementRecordID.getGeneratedId();
//                    // tambahkan data-data pada record
//                    rec.dataList.add(dtpts);
//                }
                
                // tambahkan record pada cluster
                cluster.recordList.add(rec);
            }
            
        } catch (IOException ex) {
            Logger.getLogger(CSVLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return cluster;
    }
    
}
