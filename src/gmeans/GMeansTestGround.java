/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gmeans;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;

/**
 *
 * @author root
 */
public class GMeansTestGround {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        CSVLoader csv = new CSVLoader();
        Cluster cluster = csv.loadCSVToCluster("./data/iris_id.csv");
        Utility util = new Utility();
        double[][] valueMatrix = util.convert2DListToMatrix(cluster);
        Matrix m = new Matrix(valueMatrix);
        
        EigenvalueDecomposition eig = m.eig();
        Matrix eigenValues = eig.getD();
        Matrix eigenVector = eig.getV();
        
        double[][] eigenValueArray = eigenValues.getArray();
        double[][] eigenVectorArray = eigenVector.getArray();
        
        System.out.println("eigenValueArray X size: " + eigenValueArray.length);
        System.out.println("eigenValueArray Y size: " + eigenValueArray[0].length);
        
        System.out.println("eigenVectorArray X size: " + eigenVectorArray.length);
        System.out.println("eigenVectorArray Y size: " + eigenVectorArray[0].length);
        
        System.out.println("---- EigenValue ----");
        
        for (int i = 0; i < eigenValueArray.length; i++) {
            for (int j = 0; j < eigenValueArray[i].length; j++) {
                System.out.print(eigenValueArray[i][j] + " ");
            }
            System.out.println("");
        }
        
        System.out.println("---- EigenVector ----");
        
        for (int i = 0; i < eigenVectorArray.length; i++) {
            for (int j = 0; j < eigenVectorArray[i].length; j++) {
                System.out.print(eigenVectorArray[i][j] + " ");
            }
            System.out.println("");
        }
        
        double[] fetchColumnMatrix2D = util.fetchColumnMatrix2D(eigenVectorArray, 0);
        
        System.out.println("---- Fetch ----");
        for (int i = 0; i < fetchColumnMatrix2D.length; i++) {
            System.out.print(fetchColumnMatrix2D[i] + " ");
        }
        
//        double[] realEigenvalues = eig.getRealEigenvalues();
//        double[] singularValues = m.svd().getSingularValues();
//        
//        Matrix v = eig.getV();
//        double[][] array = v.getArray();
//        
//        for (int i = 0; i < array.length; i++) {
//            for (int j = 0; j < array[i].length; j++) {
//                System.out.print(array[i][j] + " ");
//            }
//            System.out.println("");
//        }
//        
//        for (int i = 0; i < singularValues.length; i++) {
//            System.out.println("singular value " + singularValues[i]);
//        }
//        
//        for (int i = 0; i < realEigenvalues.length; i++) {
//            System.out.println("eigenvalue " + realEigenvalues[i]);
//        }
    }
    
}
