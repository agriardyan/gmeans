/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gmeans;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author root
 */
public class Record {
    
    public String recordID;
    public List<Double> dataList;  // act as SingleData class
    public String c;

    public Record() {
        dataList = new ArrayList<>();
    }

    @Override
    public String toString() {
//        String title = "Record " + recordID + " : ";
        String title = recordID;
        String content = "";
        for (int i = 0; i < dataList.size(); i++) {
            content += ", " + dataList.get(i);
        }
        return title + content;
    }
    
    
    
    
    
}
