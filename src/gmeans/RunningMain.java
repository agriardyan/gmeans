/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gmeans;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import java.util.List;
import jdistlib.disttest.NormalityTest;
import kmeans.KMeansAdHoc;

/**
 *
 * @author pacman
 */
public class RunningMain {
    
    public static void main(String[] args) {
        RunningMain r = new RunningMain();
        CSVLoader csv = new CSVLoader();
//        Cluster cluster = csv.loadCSVToCluster("./data/iter/iris_id_c2.csv");
        Cluster cluster = csv.loadCSVToCluster("./data/iter/flight_id_c1_2.csv");

        Utility util = new Utility();
        double[][] valueMatrix = util.convert2DListToMatrix(cluster);
        Matrix m = new Matrix(valueMatrix);
        
        double[] clusterCenter = {8.0, 9.0};
//        double[] clusterCenter = {778.0, 783.0};
//        double[] clusterCenter = {86.625, 90.0};
//        double[] clusterCenter = {222.33333333333334, 231.66666666666666};
//        double[] clusterCenter = {5.2, 5.0};

//        double[] clusterCenter = {5.7, 3, 4.2, 1.2};
//        double[] clusterCenter = {6.30103092783505, 2.8865979381443303, 4.958762886597939, 1.6958762886597945};
//        double[] clusterCenter = {6.3, 2.8, 5.1, 1.5};
        
//        double[] clusterCenter = {5.005660377358491, 3.360377358490567, 1.562264150943396, 0.2886792452830188};
//        double[] clusterCenter = {5, 3.4, 1.5, 0.2};
        
        cluster.centroid = util.convert1DArrayToRecord(clusterCenter);
        
        double alpha = r.step1();
        double[][] initCentroidSet = r.step2(cluster);
        Cluster[] clusterSet = r.step3(cluster, initCentroidSet[0], initCentroidSet[1]);
        double[] xiac = r.step4(util.convert1DListToArray(clusterSet[0].centroid), util.convert1DListToArray(clusterSet[1].centroid), cluster);
        
        System.out.println("---- xiac");
        for(double xic : xiac) {
            System.out.print(xic + " ");
        }
        
        System.out.println("");
        
        boolean needSplit = r.step5(xiac, alpha);
        
        if(needSplit) {
            System.out.println("not need split");
        } else {
            System.out.println("need split");
        }
        
        for (int i = 0; i < clusterSet.length; i++) {
            Cluster c = clusterSet[i];
            
            String res = c.toString();
            
            System.out.println(res);
            
        }
        
        
    }
    
    // step 1 : choose significance level (alpha)
    public double step1() {
        double alpha = 0.05;
        return alpha;
    }
    
    // step 2 : initialize two center (children of c)
    public double[][] step2(Cluster cluster) {
        Utility util = new Utility();
        double[][] valueMatrix = util.convert2DListToMatrix(cluster);
        Matrix m = new Matrix(valueMatrix);

//        double[] clusterCenter = {5.7, 3, 4.2, 1.2};
        double[] clusterCenter = util.convert1DListToArray(cluster.centroid);

        EigenvalueDecomposition eig = m.eig();
        Matrix eigenValues = eig.getD();
        Matrix eigenVectors = eig.getV();

        double[][] eigenValueArrays = eigenValues.getArray();
        double[][] eigenVectorArrays = eigenVectors.getArray();

        double eigenValue = eigenValueArrays[0][0];
        double[] eigenVector = util.fetchColumnMatrix2D(eigenVectorArrays, 0);

        Calculation calc = new Calculation();
        
        double[] initCentroid1 = calc.calculateChildCentroidPositive(clusterCenter, eigenVector, eigenValue);
        double[] initCentroid2 = calc.calculateChildCentroidNegative(clusterCenter, eigenVector, eigenValue);
        
        double[][] child = new double[2][initCentroid1.length];
        
        child[0] = initCentroid1;
        child[1] = initCentroid2;
        
        return child;
    }
    
    // step 3 : run k-means to get new centroid c1 and c2
    public Cluster[] step3(Cluster cluster, double[] initCentroid1, double[] initCentroid2) {
        KMeansAdHoc kmean = new KMeansAdHoc();
        Cluster[] runKMeans = kmean.runKMeans(cluster, initCentroid1, initCentroid2);
        return runKMeans;
    }
    
    // step 4 : let v = c1 - c2; xi' = (xi, v) / ||v||2
    public double[] step4(double[] centroid1, double[] centroid2, Cluster cluster) {
        Utility util = new Utility();
        
        Calculation calc = new Calculation();
        
        Matrix mtrC1 = new Matrix(centroid1, 1);
        Matrix mtrC2 = new Matrix(centroid2, 1);
        
        Matrix mtrV = mtrC1.minus(mtrC2);
        
        double[] v = mtrV.getArray()[0];
        
        List<Record> recordList = cluster.recordList;
        int recListSize = recordList.size();
        
        double[][] xi = new double[recListSize][recordList.get(0).dataList.size()];
        
        for (int i = 0; i < recListSize; i++) {
            xi[i] = util.convert1DListToArray(recordList.get(i));
        }
        
        double[] xiac = new double[recordList.size()];
        
        double dotProduct = 0;
        double norm = 0;
        for (int i = 0; i < xi.length; i++) {
            dotProduct = calc.dotProduct(xi[i], v);
            norm = mtrV.norm2();
            
//            xiac[i] = dotProduct / Math.pow(norm, 2);   // careful here, we dont know if it was 2norm (euclidean) or norm^2 (euc_norm^2), here is norm^2
            xiac[i] = dotProduct / norm;   // careful here, we dont know if it was 2norm (euclidean) or norm^2 (euc_norm^2), here is norm^2
        }
        
        double[] zscore = calc.zScoreNormalization(xiac);
        
        return zscore;
    }
    
    // step 5 : compare critical value with significance level (alpha)
    public boolean step5(double[] x, double alpha) {
        boolean stat = false;
        double ad = NormalityTest.anderson_darling_statistic(x);
        
        // adjustment for few datapoints
        ad = ad * (1 + (4/x.length) - (25/(Math.pow(x.length, 2))));
        
        double pValue = NormalityTest.anderson_darling_pvalue(ad, x.length);
        
        System.out.println("ad: " + ad);
        System.out.println("alpha: " + alpha);
        System.out.println("pval: " + String.format("%.2f", pValue));
        
        if(pValue >= alpha) {
            stat = true;
        } else {
            stat = false;
        }
        
        return stat;
        
        
    }
    
}
