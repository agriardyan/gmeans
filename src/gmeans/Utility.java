/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gmeans;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pacman
 */
public class Utility {

    public Utility() {
    }
    
    /**
     * Mengubah data dalam bentuk list ke tipe data array
     * 
     * usages: Dikhususkan untuk list yang berada dalam kelas Cluster (recordList), karena didalam recordList terdapat list lagi (doubleList)
     * @param cluster Obyek kelas Cluster
     * @return Array 2d hasil konversi
     */
    public double[][] convert2DListToMatrix(Cluster cluster) {
        List<Record> recordList = cluster.recordList;
                
        int rowCount = recordList.size();
        int colCount = recordList.get(0).dataList.size();
        
        double[][] matrixData = new double[rowCount][colCount];
        
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < recordList.get(i).dataList.size(); j++) {
                matrixData[i][j] = recordList.get(i).dataList.get(j);
            }
        }
        
        return matrixData;
        
    }
    
    public double[] convert1DListToArray(Record record) {
        List<Double> doubleList = record.dataList;
        double[] arrayData = new double[doubleList.size()];
        
        for (int i = 0; i < doubleList.size(); i++) {
            arrayData[i] = doubleList.get(i);
        }
        
        return arrayData;
        
    }
    
    public List<Double> convert1DArrayToList(double[] array) {
        List<Double> doubleList = new ArrayList<>();
        for (double data : array) {
            doubleList.add(data);
        }
        return doubleList;
    }
    
    /**
     * Mengkonvert array 1d ke kelas Record
     * 
     * usages: Mengisi centroid pada kelas Cluster (centroid disana didefinisikan dlm bentuk objek Record, bukan list atau array)
     * @param array
     * @return 
     */
    public Record convert1DArrayToRecord(double[] array) {
        List<Double> doubleList = new ArrayList<>();
        for (double data : array) {
            doubleList.add(data);
        }
        Record rec = new Record();
        rec.dataList = doubleList;
        return rec;
    }
    
    /**
     * Mengambil data dari dalam matrix berdasarkan kolom yang diminta
     * 
     * usages: Mengambil kolom dari matrix eigenvectors
     * @param sourceMatrix Matrix sumber yang akan difetch kolomnya
     * @param colIndex Index kolom yang diminta
     * @return Array 1 dimensi berisi data dalam kolom yang diminta
     */
    public double[] fetchColumnMatrix2D(double[][] sourceMatrix, int colIndex) {
        int length = sourceMatrix.length;
        double[] fetch = new double[length];
        
        for (int i = 0; i < length; i++) {
            fetch[i] = sourceMatrix[i][colIndex];
        }
        
        return fetch;
    }
    
}
