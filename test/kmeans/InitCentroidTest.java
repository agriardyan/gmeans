/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kmeans;

import junit.framework.TestCase;

/**
 *
 * @author pacman
 */
public class InitCentroidTest extends TestCase {

    public InitCentroidTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of median method, of class InitCentroid.
     */
    public void testMedian() {
        System.out.println("median");
        double[][] data = {{9, 2, 2, 1, 9, 6, 7, 8}, {7, 5, 9, 2, 4, 6, 1, 3}};

        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                System.out.print(data[i][j] + " ");
            }
            System.out.println("");
        }

        InitCentroid instance = new InitCentroid();
        double[] expResult = {8, 3.5, 5.5, 1.5, 6.5, 6, 4, 5.5};
        double[] result = instance.median(data);

        System.out.println("\n--- res");
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + " ");
        }

        for (int i = 0; i < expResult.length; i++) {
            assertEquals(expResult[i], result[i]);
        }
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of initCentroidFromData method, of class InitCentroid.
     */
    public void testInitCentroidFromData() {
        System.out.println("initCentroidFromData");
        double[][] data = {{7, 9, 1},
        {5, 2, 2},
        {9, 2, 2},
        {2, 1, 6},
        {4, 9, 7},
        {6, 6, 8},
        {1, 7, 9},
        {3, 8, 9}};
        
        InitCentroid instance = new InitCentroid();
        double[] expResult = {6,6};
        double[] result = instance.initCentroidFromData(data);
        
        for (int i = 0; i < expResult.length; i++) {
            assertEquals(expResult[i], result[i]);
        }
        
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

}
