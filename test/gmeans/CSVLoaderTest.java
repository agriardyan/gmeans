/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gmeans;

import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author root
 */
public class CSVLoaderTest extends TestCase {

    public CSVLoaderTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of loadCSVToCluster method, of class CSVLoader.
     */
    public void testLoadCSVToCluster() {
        System.out.println("loadCSVToCluster");
        String path = "./data/iris.csv";
        CSVLoader instance = new CSVLoader();
        Cluster expResult = null;
        Cluster result = instance.loadCSVToCluster(path);

        List<Record> recordList = result.recordList;
        List<Double> doubleList = recordList.get(0).dataList;
        double[] doubleArray = {5.1, 3.5, 1.4, 0.2, 1};        
        
        for (int i = 0; i < doubleList.size(); i++) {
            assertEquals(doubleArray[i], doubleList.get(i));
        }
        
        for (int i = 0; i < recordList.size(); i++) {
            for (int j = 0; j < recordList.get(i).dataList.size(); j++) {
                System.out.print(recordList.get(i).dataList.get(j) + " ");
            }
            System.out.println("");
        }
        
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

}
