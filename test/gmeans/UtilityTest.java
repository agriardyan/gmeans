/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gmeans;

import junit.framework.TestCase;

/**
 *
 * @author pacman
 */
public class UtilityTest extends TestCase {
    
    public UtilityTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of convert2DListToMatrix method, of class Utility.
     */
    public void testConvert2DListToMatrix() {
        System.out.println("convert2DListToMatrix");
        CSVLoader csvLoad = new CSVLoader();
        Cluster cluster = csvLoad.loadCSVToCluster("./data/iris.csv");
        Utility instance = new Utility();
        double[][] expResult = null;
        double[][] result = instance.convert2DListToMatrix(cluster);
        double[] testArray = {5.1, 3.5, 1.4, 0.2, 1.0};
        double[] firstRow = result[0];
        
//        for (int i = 0; i < result[0].length; i++) {
//            System.out.println("i : " + i + " res : " + result[0][i] + " ");
//            System.out.println("i : " + i + " tes : " + testArray[i] + " ");
//        }
        for (int i = 0; i < firstRow.length; i++) {
            assertEquals(firstRow[i], testArray[i]);
        }
        
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
}
